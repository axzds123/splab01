//
// Created by Zombie on 2020/11/4.
//

#ifndef SPLAB1_EMPMGR_H
#define SPLAB1_EMPMGR_H

#endif //SPLAB1_EMPMGR_H

#define MAXNAME 64
#define MAXID 64
#define MAXBALANCE 64

typedef struct  {
    int id;   /* a C-style string (array of chars) */
    char first_name[MAXNAME];
    char last_name[MAXNAME];
    int balance;
}EMPLOYEE;

typedef struct _EMPNODE {
    EMPLOYEE* data;
    struct _EMPNODE* last;
    struct _EMPNODE* next;
} EMPNODE;

int emp_init_db(char* path);
void emp_uninit_db();
void emp_insert_node(EMPLOYEE* emp);
void emp_serilize_db(char* data);
void emp_find_info_by_id(char* data, int id);
void emp_find_info_by_lastname(char* data, char* lastname);
int emp_save_db(char*);
void emp_serilize_db_and_sort_by_id(char* data , int desc);