//
// Created by Zombie on 2020/11/4.
//

#include "empmgr.h"
#include "readfile.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

EMPNODE g_root = {NULL, NULL};

int emp_init_db(char* path)
{
    open_file(path);

    int flag = 0;

    while(1)
    {
        int id = 0;   /* a C-style string (array of chars) */
        char first_name[MAXNAME];
        char last_name[MAXNAME];
        int balance = 0.0;

        memset(first_name,0, MAXNAME);
        memset(last_name, 0 , MAXNAME);

        if (read_int(&id) != 0)
            break;
        if(read_string(first_name) != 0)
            break;
        if (read_string(last_name) != 0)
            break;
        if (read_int(&balance) != 0)
            break;

        EMPLOYEE* p = (struct EMPLOYEE*)malloc(sizeof(EMPLOYEE));

        p->id = id;
        strncpy(p->first_name , first_name ,MAXNAME);
        strncpy(p->last_name , last_name ,MAXNAME);
        p->balance = balance;

        emp_insert_node(p);
    }

    close_file();

    return 0;
}

void emp_insert_node(EMPLOYEE* emp)
{
    EMPNODE* cur = g_root.next;
    EMPNODE* last = &g_root;

    while(cur != NULL)
    {
        last = cur;
        cur = cur->next;
    }

    EMPNODE* node = (struct EMPNODE*)malloc(sizeof(EMPNODE));

    node->data = emp;
    node->last = last;
    node->next = NULL;

    last->next = node;
}

void emp_clear_node()
{
    EMPNODE* cur = g_root.next;
    EMPNODE* last = &g_root;

    //pos last node
    while(cur != NULL)
    {
        last = cur;
        cur = cur->next;
    }

    cur = last;
    last = last->last;
    while (last != NULL)
    {
        free(cur->data);
        free(cur);
        cur = last;
        last = last->last;
    }
    cur->next = NULL;
}

void emp_serilize_db(char* data)
{
    EMPNODE* cur = g_root.next;
    EMPNODE* last = &g_root;

    //pos last node
    while(cur != NULL)
    {
        char tmp[1024];
        sprintf(tmp , "%d %s %s %d\n" , cur->data->id, cur->data->first_name, cur->data->last_name, cur->data->balance);
        strcat(data, tmp);

        last = cur;
        cur = cur->next;
    }
}

#define MAX_DATA 1024

typedef struct _EMP_SORT_NODE{
    int id ;
    char data[MAX_DATA] ;
    struct _EMP_SORT_NODE* last;
    struct _EMP_SORT_NODE* next;
} EMP_SORT_NODE;

static void emp_insert_sort_node(EMP_SORT_NODE* root , int id , char* data , int desc)
{
    EMP_SORT_NODE *node = (EMP_SORT_NODE*)malloc(sizeof(EMP_SORT_NODE));

    node->id = id;
    node->last = NULL;
    node->next = NULL;
    memset(node->data, 0 , MAX_DATA);
    strcpy(node->data , data);

    EMP_SORT_NODE* cur = root->next;
    EMP_SORT_NODE* last = root;

    while (cur != NULL)
    {
        if ((desc == 0) && (cur->id > id) )
            break;
        if ((desc == 1) && (cur->id < id) )
            break;

        last = cur;
        cur = cur->next;
    }

    if(cur == NULL)
    {
        last->next = node;
        node->last = last;
        return;
    }

    node->next = cur;
    node->last = cur->last;

    cur->last->next = node;
    cur->last = node;
}

static void emp_clear_sort_node(EMP_SORT_NODE* root)
{
    EMP_SORT_NODE* cur = root->next;
    EMP_SORT_NODE* last = &root;

    while(cur != NULL)
    {
        last = cur;
        cur = cur->next;
    }

    cur = last;
    last = last->last;
    while (last != NULL)
    {
        free(cur);
        cur = last;
        last = last->last;
    }
    cur->next = NULL;
}

void emp_serilize_db_and_sort_by_id(char* data , int desc)
{
    EMP_SORT_NODE *root = (EMP_SORT_NODE*)malloc(sizeof(EMP_SORT_NODE));

    root->id = 0;
    memset(root->data,0 ,MAX_DATA);
    root->next = NULL;
    root->last = NULL;

    EMPNODE* cur = g_root.next;
    EMPNODE* last = &g_root;

    //pos last node
    while(cur != NULL)
    {
        char tmp[1024];
        sprintf(tmp , "%14d| %13s| %13s| %14d\n" , cur->data->id, cur->data->first_name, cur->data->last_name, cur->data->balance);
        emp_insert_sort_node(root, cur->data->id, tmp , desc);

        last = cur;
        cur = cur->next;
    }

    //serilize
    EMP_SORT_NODE* _cur = root->next;

    while(_cur != NULL)
    {
        strcat(data, _cur->data);
        _cur = _cur->next;
    }

    emp_clear_sort_node(root);
    free(root);
}

void emp_find_info_by_id(char* data, int id)
{
    EMPNODE* cur = g_root.next;
    EMPNODE* last = &g_root;

    while(cur != NULL)
    {
        if(cur->data->id == id)
        {
            char tmp[1024];
            sprintf(tmp, "%14d| %13s| %13s| %14d\n", cur->data->id, cur->data->first_name, cur->data->last_name,
                    cur->data->balance);
            strcat(data, tmp);
            return;
        }

        last = cur;
        cur = cur->next;
    }
}

void emp_find_info_by_lastname(char* data, char* lastname)
{
    EMPNODE* cur = g_root.next;
    EMPNODE* last = &g_root;

    while(cur != NULL)
    {
        if(strcmp(cur->data->last_name , lastname) == 0)
        {
            char tmp[1024];
            sprintf(tmp, "%14d| %13s| %13s| %14d\n", cur->data->id, cur->data->first_name, cur->data->last_name,
                    cur->data->balance);
            strcat(data, tmp);
        }

        last = cur;
        cur = cur->next;
    }
}

int emp_save_db(char* path)
{
    char tmp[1024] = {0};

    emp_serilize_db(tmp);

    return write_and_flush_file(path,tmp, strlen(tmp));
}

void emp_uninit_db()
{
    emp_clear_node();
}

