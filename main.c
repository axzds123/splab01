#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "readfile.h"
#include "empmgr.h"

void Usage()
{
    printf("---------------------------------\n");
    printf("1. Print the Database\n");
    printf("2. Lookup employee by ID\n");
    printf("3. Lookup employee by last name\n");
    printf("4. Add an Employee\n");
    printf("5. Quit\n");
    printf("---------------------------------\n");
}

static char* g_dbpath = NULL;

void print_t()
{
    printf("--------------|--------------|--------------|---------------\n");
    printf("  ID          |   First Name | Last Name    | Balance       \n");
    printf("--------------|--------------|--------------|---------------\n");
}

void print_database()
{
    emp_init_db(g_dbpath);
    char tmp[1024] = {0};

    emp_serilize_db_and_sort_by_id(tmp, 0);

    print_t();

    printf(tmp);

    printf("--------------|--------------|--------------|---------------\n");

    emp_uninit_db();
}

void find_emp_by_id()
{
    int id = 0;
    printf("input id : ");
    scanf("%d", &id);

    if(id == 0 )
    {
        printf("id faild !\n");
        return;
    }

    emp_init_db(g_dbpath);

    char tmp[1024] = {0};

    emp_find_info_by_id(tmp, id);

    print_t();

    printf(tmp);

    printf("--------------|--------------|--------------|---------------\n");

    emp_uninit_db();
}

void find_emp_by_lastname()
{
    char lastname[MAXNAME] = {0};
    printf("input lastname : ");
    scanf("%s", &lastname);

    if(strlen(lastname) == 0 )
    {
        printf("lastname faild !\n");
        return;
    }

    emp_init_db(g_dbpath);

    char tmp[1024] = {0};

    emp_find_info_by_lastname(tmp, lastname);

    if (strlen(tmp) == 0)
        printf("not found lastname!\n");
    else
    {
        print_t();

        printf(tmp);

        printf("--------------|--------------|--------------|---------------\n");
    }

    emp_uninit_db();
}

void add_emp()
{
    emp_init_db(g_dbpath);

    int id = 0;
    char firstname[MAXNAME] = {0};
    char lastname[MAXNAME] = {0};
    int balance = 0;

    printf("input id : ");
    scanf("%d", &id);

    printf("input first name : ");
    scanf("%s", &firstname);

    printf("input last name : ");
    scanf("%s", &lastname);

    printf("input balance : ");
    scanf("%d", &balance);

    EMPLOYEE *emp = (EMPLOYEE*) malloc(sizeof(EMPLOYEE));

    emp->id = id;
    strcpy(emp->last_name , lastname);
    strcpy(emp->first_name, firstname);
    emp->balance = balance;

    emp_insert_node(emp);

    emp_save_db(g_dbpath);

    emp_uninit_db();
}

int main(int argc , char **argv) {
    if (argc < 2) {
        printf("Please give a file path!\n");
        return 0;
    }

    g_dbpath = argv[1];

    while(1)
    {
        Usage();

        int flag = 0;
        printf("input a number : ");
        scanf("%d", &flag);

        switch (flag)
        {
            case 1:
                print_database();
                break;
            case 2:
                find_emp_by_id();
                break;
            case 3:
                find_emp_by_lastname();
                break;
            case 4:
                add_emp();
                break;
            case 5:
                break;
            default:
                printf("input faild , please repeat input 0-5 ! \n");
                break;
        }

        if (flag == 5)
            break;
    }

    return 0;
}
